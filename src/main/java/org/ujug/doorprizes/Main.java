package org.ujug.doorprizes;

import java.io.IOException;
import java.security.SecureRandom;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Properties;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class Main {
    public static void main(String ... args) throws IOException {
        final Log log = LogFactory.getLog(Main.class);

        log.debug("Loading properties");
        Properties config = new Properties();
        config.load(Objects.requireNonNull(Main.class.getClassLoader().getResourceAsStream("doorprize.properties")));

        List<String> prizes = Arrays.asList(config.getProperty("prizes").split(","));

        log.debug("Prizes: " + prizes);

        EntrantRetriever entrantRetriever = new GmailEntrantRetriever(config);
        List<String> entrants = entrantRetriever.getEntrants();

        final SecureRandom rng = new SecureRandom();
        final Map<String, String> prizeToWinner = new HashMap<>(prizes.size());

        try {
            prizes.forEach(s -> prizeToWinner.put(s, entrants.remove(rng.nextInt(entrants.size()))));
        } catch (IllegalArgumentException ignored) {
            // Too many prizes, not enough people
        }

        System.out.println("================== Winners ==================");
        prizeToWinner.entrySet().forEach(System.out::println);
    }
}
