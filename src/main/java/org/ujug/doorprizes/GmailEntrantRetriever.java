package org.ujug.doorprizes;

import java.io.IOException;
import java.io.StringReader;
import java.security.GeneralSecurityException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.stream.Collectors;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.store.FileDataStoreFactory;
import com.google.api.services.gmail.Gmail;
import com.google.api.services.gmail.GmailScopes;
import com.google.api.services.gmail.model.Label;
import com.google.api.services.gmail.model.ListMessagesResponse;
import com.google.api.services.gmail.model.Message;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * EntrantRetriever instance using GMail.
 * Expects the following config properties to be in place:
 * - credentials: a JSON string of OAuth values from Google
 * - gmail_label_id: Label ID from GMail to search, defaults to empty string
 * - gmail_user: Google User (gmail address) to use, defaults to "me"
 */
public class GmailEntrantRetriever implements EntrantRetriever {
    private static final String APPLICATION_NAME = "ujug-doorprize";
    private static final JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();
    private static final String TOKENS_DIRECTORY_PATH = "tokens";

    /**
     * Global instance of the scopes required by this quickstart.
     * If modifying these scopes, delete your previously saved tokens/ folder.
     */
    private static final List<String> SCOPES = Collections.singletonList(GmailScopes.GMAIL_READONLY);
    private static final String CREDENTIALS = "credentials";
    private static final String GMAIL_LABEL_ID = "gmail_label_id";
    private static final String GMAIL_USER = "gmail_user";

    private Properties config;
    private Log log;

    public GmailEntrantRetriever(Properties config) {
        this.config = config;
        this.log = LogFactory.getLog(GmailEntrantRetriever.class);
    }

    /**
     * Creates an authorized Credential object.
     * @param HTTP_TRANSPORT The network HTTP Transport.
     * @return An authorized Credential object.
     * @throws IOException If the credentials.json file cannot be found.
     */
    private Credential getCredentials(final NetHttpTransport HTTP_TRANSPORT) throws IOException {
        this.log.info("Obtaining credentials");
        // Load client secrets.
        GoogleClientSecrets clientSecrets = GoogleClientSecrets.load(JSON_FACTORY,
                new StringReader(this.config.getProperty(CREDENTIALS, "{}")));

        // Build flow and trigger user authorization request.
        GoogleAuthorizationCodeFlow flow = new GoogleAuthorizationCodeFlow.Builder(
                HTTP_TRANSPORT, JSON_FACTORY, clientSecrets, SCOPES)
                .setDataStoreFactory(new FileDataStoreFactory(new java.io.File(TOKENS_DIRECTORY_PATH)))
                .setAccessType("offline")
                .build();
        LocalServerReceiver receiver = new LocalServerReceiver.Builder().setPort(8899).build();
        return new AuthorizationCodeInstalledApp(flow, receiver).authorize("user");
    }

    @Override
    public List<String> getEntrants() {
        this.log.info("Finding entrants from Gmail");
        Set<String> entrants = new HashSet<>();

        // Build a new authorized API client service.
        final NetHttpTransport HTTP_TRANSPORT;
        try {
            this.log.debug("Getting Gmail service");
            HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
            Gmail service = new Gmail.Builder(HTTP_TRANSPORT, JSON_FACTORY, getCredentials(HTTP_TRANSPORT))
                    .setApplicationName(APPLICATION_NAME)
                    .build();

            String user = this.config.getProperty(GMAIL_USER, "me");

            // Print Messages from UJUG Label
            this.log.debug(String.format("Getting messages - Label: %s", this.config.getProperty(GMAIL_LABEL_ID)));
            Label label = service.users().labels().get(user, this.config.getProperty(GMAIL_LABEL_ID)).execute();
            Gmail.Users.Messages.List messagesList = service.users().messages().list(user);
            messagesList = messagesList.setLabelIds(
                    Collections.singletonList(this.config.getProperty(GMAIL_LABEL_ID)))
                    .setQ(String.format("after:%s label:%s is:unread",
                        LocalDate.now().minusDays(1).format(DateTimeFormatter.ISO_LOCAL_DATE),
                        label.getName()));

            ListMessagesResponse listMessagesResponse = messagesList.execute();

            this.log.info(String.format("Estimated results: %d", listMessagesResponse.getResultSizeEstimate()));

            if (listMessagesResponse.getResultSizeEstimate() > 0) {
                List<Message> messages = listMessagesResponse.getMessages();

                for (Message m : messages) {
                    Message fullMessage = service.users().messages().get(user, m.getId())
                            .setFormat("metadata").execute();

                    entrants.addAll(fullMessage.getPayload().getHeaders().stream()
                            .filter(header -> "From".equals(header.getName()))
                            .map(h -> String.format("%s", h.getValue()))
                            .collect(Collectors.toSet()));
                }
            } else {
                System.out.println(listMessagesResponse.toPrettyString());
            }
        } catch (GeneralSecurityException | IOException e) {
            e.printStackTrace();
        }

        this.log.info("Final number of results: " + entrants.size());
        return new ArrayList<>(entrants);
    }
}
