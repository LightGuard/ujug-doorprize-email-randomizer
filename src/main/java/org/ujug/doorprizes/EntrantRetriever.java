package org.ujug.doorprizes;

import java.util.List;

public interface EntrantRetriever {
    List<String> getEntrants();
}
